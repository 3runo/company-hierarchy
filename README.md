# README #

This is an Application Test

Check the video [Vimeo](https://vimeo.com/194512546) 

password : **hi-check-me**

### What is this repository for? ###

This project was created as a test

### How do I get set up? ###

* npm install
* npm run start

### Dependencies ###

* ReactJs
* Redux
* Flow type checker
* React DnD
* Lodash
* Babel, Webpack, StandardJs ...
* Mocha, Chai ...

### How to run tests ###

* npm run testdev

### About Styles ###

Hi Arseniy, I decided to keep pure CSS because I believe there was not a lot of complexity in this task related to style, even because I decided to make use of the bootstrap.

I think the style is well organized.

>`/company-hierarchy/style/sticky-footer-navbar.css`

>`/company-hierarchy/style/style.css`

I hope you'll forgive me for that.