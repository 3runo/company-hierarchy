import { renderComponent, expect } from '../test_helper'
import Header from './../../src/components/Header'

describe('- Header component', () => {
  let component

  beforeEach(() =>
    component = renderComponent(Header))

  it('-- Should contains an image with Personio logo', () =>
    expect(component.find('img'))
      .to.have.prop('src').match(/personio-logo/))

  it('-- Should contains navbar', () =>
    expect(component.find('nav.navbar')).to.exist)

  it('-- Should contains a "Home" link', () =>
    expect(component.find('a.nav-link'))
      .to.contain('Home'))

  it('-- Should contains a "Select file" link', () =>
    expect(component.find('a.nav-link'))
      .to.contain('Select file'))

  it('-- Should contains a "Organogram" link', () =>
    expect(component.find('a.nav-link'))
      .to.contain('Organogram'))
})
