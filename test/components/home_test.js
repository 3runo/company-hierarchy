import { renderComponent, expect } from '../test_helper'
import Home from './../../src/components/Home'

describe('- Home component', () => {
  let component

  beforeEach(() =>
    component = renderComponent(Home))

  it('-- Should have an Header1 Element with "Company Hierarchy" text', () =>
    expect(component.find('h1'))
      .to.contain('Company Hierarchy'))
})
