import { renderComponent, expect } from '../test_helper'
import App from './../../src/components/App'

describe('- App component', () => {
  let component

  beforeEach(() =>
    component = renderComponent(App))

  it('-- Should contains a .container element in order to render children', () =>
    expect(component.find('.container')).to.exist)
})
