import { renderComponent, expect } from '../test_helper'
import Upload from './../../src/components/Upload'

describe('- Upload component with empty organogram', () => {
  let component

  beforeEach(() =>{
    const props = { organogram: {} }
    const state = { userMessage: 'Hi I am an error message' }
    component = renderComponent(Upload, props, state)
  })

  it('-- Should have an Header1 Element with "Manage organization file" text', () =>
    expect(component.find('h1'))
      .to.contain('Manage organization file'))

  it('-- Should have an input[type=file] to upload our JSON data', () =>
    expect(component.find('input[type=file]')).to.extist)

  it('-- Should have an Error message', () =>
    expect(component.find('.alert-danger')).to.extist)
})

describe('- Upload component with valid organogram', () => {
  let component

  beforeEach(() =>{
    component = renderComponent(Upload)
  })

  it('-- Should contain an remove JSON file <buttom>', () =>
    expect(component.find('button'))
      .to.contain('Remove organogram file'))

  it('-- Should contain a "Go to my organogram" link', () =>
    expect(component.find('a.btn'))
      .to.contain('Go to my organogram'))

})
