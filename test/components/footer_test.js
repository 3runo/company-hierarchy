import { renderComponent, expect } from '../test_helper'
import Footer from './../../src/components/Footer'

describe('- Footer component', () => {
  let component

  beforeEach(() =>
    component = renderComponent(Footer))

  it('-- Should a Personio link', () =>
    expect(component.find('a'))
      .to.have.prop('href', 'https://www.personio.de/en/'))
})
