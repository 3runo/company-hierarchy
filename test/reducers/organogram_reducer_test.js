import { expect } from '../test_helper'
import OrganogramReducer, { initialState } from './../../src/store/reducers/organogram-reducer'
import { basicOrganogramStructure } from './../../src/assets/basic-organogram'
import { ACTIONS } from './../../src/store/actions/'

const testValue = {
  'Test': {
    'position': 'Fronted Engineer',
    'employees': []
  }
}

describe('- Organogram Reducer', () => {
  it('-- Should handle with unknown types', () => {
    expect(OrganogramReducer(undefined, {}))
      .to.deep.equal(initialState)
  })

  it('-- Handles CLEAR_ORGANOGRAM action', () => {
    expect(OrganogramReducer({}, ACTIONS.CLEAR_ORGANOGRAM))
      .to.eql(initialState)
  })

  it('-- Handles SET_ORGANOGRAM action', () => {
    expect(OrganogramReducer(testValue, ACTIONS.SET_ORGANOGRAM))
      .to.have.any.keys('organogram', 'Test')
  })
})
