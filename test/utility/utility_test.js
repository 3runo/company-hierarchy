import { expect } from '../test_helper'
import isEmptyObject from './../../src/utility/isEmptyObject'
import isValidStrJSON from './../../src/utility/isValidStrJSON'
import { basicOrganogramStructure } from './../../src/assets/basic-organogram'
import { getEmployeeListByName, removeEmployeeByName, pushEmployee, flatOrganogramToArray } from './../../src/utility/organogramHelpers'

const testValue = {
  'Test': {
    'position': 'Fronted Engineer',
    'employees': []
  }
}

// isEmptyObject()
describe('- helper function: isEmptyObject()', () => {
  it('-- Handles with null value', () =>
    expect(isEmptyObject(null))
      .to.deep.false)

  it('-- Handles with undefined value', () =>
    expect(isEmptyObject(undefined))
      .to.deep.false)

  it('-- Handles with {a:1} value', () =>
    expect(isEmptyObject({a:1}))
      .to.deep.false)

  it('-- Handles with {} value', () =>
    expect(isEmptyObject({}))
      .to.deep.true)
})

// isValidStrJSON()
describe('- helper function: isValidStrJSON()', () => {
  it('-- Handles with null value', () =>
    expect(isValidStrJSON(null))
      .to.deep.false)

  it('-- Handles with undefined value', () =>
    expect(isValidStrJSON(undefined))
      .to.deep.false)

  it('-- Handles with "aaa" value', () =>
    expect(isValidStrJSON('aaa'))
      .to.deep.false)

  it('-- Handles with \'{"a":1}\' value', () =>
    expect(isValidStrJSON('{"a":1}'))
      .to.deep.true)
})

// getEmployeeListByName()
describe('- helper function: getEmployeeListByName()', () => {
  it('-- Find Barbara she exsists', () =>
    expect(getEmployeeListByName(basicOrganogramStructure, 'Barbara'))
      .to.be.instanceof(Array)
      .to.have.length.above(0))

  it('-- Find BarbarO it does not exists', () =>
    expect(getEmployeeListByName(basicOrganogramStructure, 'BarbarO'))
      .to.be.instanceof(Array)
      .to.have.lengthOf(0))
})

// removeEmployeeByName()
describe('- helper function: removeEmployeeByName()', () => {
  it('-- Removing "David"', () =>
    expect(removeEmployeeByName(basicOrganogramStructure, 'David'))
      .to.not.have.any.keys('David'))
})

// pushEmployee()
describe('- helper function: pushEmployee()', () => {
  it('-- Adding "Test" into "David"', () =>
    expect(pushEmployee(basicOrganogramStructure, testValue, 'David'))
      .to.have.any.keys('David', 'Test'))
})

// flatOrganogramToArray()
describe('- helper function: flatOrganogramToArray()', () => {
  it('-- Transform organogram Object into a flat Array', () =>
    expect(flatOrganogramToArray(basicOrganogramStructure))
      .to.be.instanceof(Array)
      .to.have.length.above(1))
})