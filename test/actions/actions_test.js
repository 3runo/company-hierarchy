import { expect } from '../test_helper'
import { ACTIONS, setOrganogram, clearOrganogram } from './../../src/store/actions/'
import { initialState } from './../../src/store/reducers/organogram-reducer'

const testValue = {
  'Test': {
    'position': 'Fronted Engineer',
    'employees': []
  }
}

describe('- Actions', () => {
  it('-- Should reset the organogram state', () => {
    const action = clearOrganogram()
    expect(action.type).to.deep.equal(ACTIONS.CLEAR_ORGANOGRAM)
    expect(action.payload).to.deep.equal({})
  })

  it('-- Should change the organogram state', () => {
    const action = setOrganogram(testValue)
    expect(action.type).to.deep.equal(ACTIONS.SET_ORGANOGRAM)
    expect(action.payload).to.have.any.keys('Test')
  })
})