/* @flow */
export default function isEmptyObject (obj: Object): boolean {
  if (typeof obj === 'undefined' || obj === null) {
    return false
  }

  return Boolean(Object.keys(obj).length === 0 && obj.constructor === Object)
}
