/* @flow */
import { isObject, forOwn, omit, get, set } from 'lodash'
import isEmptyObject from './../utility/isEmptyObject'

import type { TOrganogram } from './../types/'

// Returns a list of objects that have the name defined in the parameter
export function getEmployeeListByName (organogram: TOrganogram, name: string) {
  return findValuesHelper(organogram, name, [])
}

function findValuesHelper (obj: Object, name: string, list) {
  if (!obj) {
    return list
  }

  if (Array.isArray(obj)) {
    for (const prop in obj) {
      list = list.concat(findValuesHelper(obj[prop], name, []))
    }

    return list
  }

  if (obj[name]) {
    list.push({ [name]: obj[name] })
  }

  if ((typeof obj === 'object') && (obj !== null)) {
    const children = Object.keys(obj)
    if (children.length > 0) {
      for (let prop = 0; prop < children.length; prop++) {
        list = list.concat(findValuesHelper(obj[children[prop]], name, []))
      }
    }
  }

  return list
}

// Removes an object from a nested object
export function removeEmployeeByName (organogram: TOrganogram, names: string | string[]) {
  function omitDeepOnOwnProps (obj) {
    if (!Array.isArray(obj) && !isObject(obj)) {
      return obj
    }

    if (Array.isArray(obj)) {
      return removeEmployeeByName(obj, names)
    }

    const o = {}
    forOwn(obj, (value, key) => {
      o[key] = removeEmployeeByName(value, names)
    })

    return omit(o, names)
  }

  if (typeof organogram === 'undefined') {
    return {}
  }

  if (Array.isArray(organogram)) {
    return organogram
      .map(omitDeepOnOwnProps)
      .filter((item) => !isEmptyObject(item))
  }

  return omitDeepOnOwnProps(organogram)
}

// Applies the removed item to a new structure
export function pushEmployee (organogram: TOrganogram, newEmployee: TOrganogram, bossName: string) {
  const flatStr = flatOrganogramToArray(organogram)
    .find((sentence) =>
      sentence.indexOf(bossName) !== -1) || ''

  const index = (flatStr.indexOf(bossName) + bossName.length)
  const path = flatStr.substr(0, index)
    .replace(/.\d./g, '[$&]') // '...property.0.' -> '...property[.0.]'
    .replace(/(\[.)/g, '[')  // '...property[.0.]' -> '...property[0.]'
    .replace(/(.])/g, '].') + '.employees' // '...property[0.]' -> '...property[0].name.employees'

  const newStructure = [...get(organogram, path, []), newEmployee]

  return set(organogram, path, newStructure)
}

// Transforms the organogram into an array
export function flatOrganogramToArray (obj: Object) {
  if (typeof (obj) !== 'object') {
    return [obj]
  }

  const result = []

  Object.keys(obj).forEach((key) => {
    if (obj[key]) {
      const chunk = flatOrganogramToArray(obj[key])
      chunk.map((item) => result.push(`${key}.${item}`))
    } else {
      result.push(key)
    }
  })

  return result
}
