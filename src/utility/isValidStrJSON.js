/* @flow */
export default function isValidStrJSON (str: string) {
  if (typeof str !== 'string' || str === null) {
    return false
  }

  try {
    window.JSON.parse(str)
  } catch (e) {
    return false
  }

  return true
}
