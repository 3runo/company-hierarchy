/* @flow */
import { ACTIONS } from '../actions'
import { basicOrganogramStructure } from './../../assets/basic-organogram'
import type { TOrganogram } from './../../types/'

type TState = {
  organogram: TOrganogram
}

export const initialState: TState = {
  organogram: basicOrganogramStructure
}

export default function organogramReducer (state: TState = initialState, action: TAction) {
  const {type, payload} = action

  if (payload === undefined) {
    return initialState
  }

  switch (type) {
    case ACTIONS.SET_ORGANOGRAM:
    case ACTIONS.CLEAR_ORGANOGRAM:
      return { ...state, organogram: payload }
  }

  return state
}
