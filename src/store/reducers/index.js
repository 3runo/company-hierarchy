/* @flow */
import {combineReducers} from 'redux'
import OrganogramReducer from './organogram-reducer'

const combinedReducers = combineReducers({
  organogramReducer: OrganogramReducer
})

export default combinedReducers
