/* @flow */
import type { TOrganogram } from './../../types/'

export const ACTIONS = {
  SET_ORGANOGRAM: 'SET_ORGANOGRAM',
  CLEAR_ORGANOGRAM: 'CLEAR_ORGANOGRAM'
}

export function setOrganogram (organogram: TOrganogram) {
  return {
    type: ACTIONS.SET_ORGANOGRAM,
    payload: organogram
  }
}

export function clearOrganogram () {
  return {
    type: ACTIONS.CLEAR_ORGANOGRAM,
    payload: {}
  }
}
