/* @flow */
import React from 'react'

export default class Footer extends React.Component {
  render () {
    return (
      <footer className="footer text-xs-center">
        <div className="container">
          <div role="contentinfo">© <a href="https://www.personio.de/en/" target="_blank">Personio GmbH</a></div>
        </div>
      </footer>
    )
  }
}
