/* @flow */
import React from 'react'
import { DragSource, DropTarget } from 'react-dnd'
import { getEmployeeListByName } from './../utility/organogramHelpers'

import type {TOrganogram} from './../types/'

type TProps = {
  key?: number,
  index: string,
  name: string,
  position: string,
  employees: TOrganogram[],
  onUpdate: Function,
  connectDragSource?: Function,
  isDragging?: boolean,
  connectDropTarget?: Function,
  isOver?: boolean
}

// Drag and drop setup
const DND_EMPLOYEE_LIST = Symbol('DND_EMPLOYEE_LIST')
const dragSourceMethods = {
  beginDrag (props) {
    return {
      draggedIndex: props.index,
      draggedEmployees: props.employees,
    }
  }
}
const dragTargetMethods = {
  drop (props, monitor, component) {
    if (monitor.didDrop()) {
      return
    }

    // Call onUpdate() method in order to modify the organogram object
    const draggedIndex = monitor.getItem().draggedIndex
    const draggedEmployees = monitor.getItem().draggedEmployees
    const parentIndex = props.index
    const selfReferencesFound = getEmployeeListByName(draggedEmployees, parentIndex)

    if (selfReferencesFound.length) {
      return
    }

    props.onUpdate(draggedIndex, parentIndex)
  },
  canDrop (props, monitor) {
    return monitor.getItem().draggedIndex !== props.index
  }
}

function employeeFactory () {
  // Drag'n drop target decorator
  @DropTarget(DND_EMPLOYEE_LIST, dragTargetMethods, (connect, monitor) => ({
    connectDropTarget: connect.dropTarget(),
    isOver: monitor.isOver()
    // isOver: monitor.isOver({ shallow: true })
  }))
  // Drag'n drop source decorator
  @DragSource(DND_EMPLOYEE_LIST, dragSourceMethods, (connect, monitor) => ({
    connectDragSource: connect.dragSource(),
    isDragging: monitor.isDragging()
  }))
  // Employee component
  class Employee extends React.Component {
    static propTypes = {
      index: React.PropTypes.string.isRequired,
      name: React.PropTypes.string.isRequired,
      position: React.PropTypes.string.isRequired,
      employees: React.PropTypes.array.isRequired,
      onUpdate: React.PropTypes.func.isRequired,
      // Drag'n drop source properties
      connectDragSource: React.PropTypes.func.isRequired,
      isDragging: React.PropTypes.bool.isRequired,
      // Drag'n drop target properties
      connectDropTarget: React.PropTypes.func.isRequired,
      isOver: React.PropTypes.bool.isRequired
    }

    props: TProps

    state = {
      isListOpen: true
    }

    // Renders
    renderEmployeeList (employeeList: TOrganogram[]) {
      return (
        employeeList.map((employee, i) => {
          const name = Object.keys(employee)[0]

          return React.createElement(employeeFactory(), {
            key: i,
            index: name,
            name: name,
            position: employee[name].position,
            employees: employee[name].employees,
            onUpdate: this.props.onUpdate
          })
        })
      )
    }

    render () {
      const {
        name,
        position,
        employees,
        // Drag'n drop source properties
        connectDragSource,
        isDragging,
        // Drag'n drop target properties
        connectDropTarget,
        isOver
      } = this.props
      const {isListOpen} = this.state
      const employeeList = Array.isArray(employees) && employees.length ? employees : []

      if (!name || !connectDragSource || !connectDropTarget) {
        return null
      }

      return connectDragSource(connectDropTarget(
        <ul className={`employee-list${isDragging ? ' is-dragging' : ''}${isOver ? ' is-over' : ''}`}>
          <li className={`employee-description${isListOpen ? ' open' : ''}`} onClick={() => this.setState({ isListOpen: !isListOpen })}>
            <div>
              <strong>{name}</strong>
            </div>
            <div>
              <span>{position}</span>
            </div>
          </li>
          <li className="">
            {this.renderEmployeeList(employeeList)}
          </li>
        </ul>
      ))
    }
  }

  return Employee
}

export default employeeFactory()
