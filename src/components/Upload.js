/* @flow */
import React from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router'
import { setOrganogram, clearOrganogram } from './../store/actions/'
import isValidStrJSON from './../utility/isValidStrJSON'
import isEmptyObject from './../utility/isEmptyObject'

@connect(
  (state) => ({
    organogram: state.organogramReducer.organogram
  }),
  { setOrganogram, clearOrganogram }
)
export default class Upload extends React.Component {
  static propTypes = {
    organogram: React.PropTypes.object.isRequired,
    setOrganogram: React.PropTypes.func.isRequired,
    clearOrganogram: React.PropTypes.func.isRequired
  }

  state = {
    userMessage: '',
    tempOrganogram: null
  }

  // Select file methods
  hasFileReaderSupport = () => window.File && window.FileReader && window.FileList && window.Blob

  onInputFileChange = (event: Event) => {
    const target = event.target
    const files = target instanceof HTMLInputElement ? target.files : undefined

    if (!(files instanceof FileList) || !this.hasFileReaderSupport()) {
      this.setState({ userMessage: 'The File APIs are not fully supported in this browser.' })
      return
    }

    const file = files[0]
    const fileReader = new FileReader()
    fileReader.onload = this.fileReaderOnLoad
    fileReader.readAsText(file)
  }

  fileReaderOnLoad = (reader: Event) => {
    const target = reader.target
    const result = target instanceof FileReader ? target.result : undefined

    if (result && typeof result === 'string' && isValidStrJSON(result)) {
      this.setState({
        userMessage: '',
        tempOrganogram: JSON.parse(result)
      })
      return
    }

    this.setState({
      userMessage: 'Um, the selected file is not a valid JSON file.',
      tempOrganogram: null
    })
  }

  // ...
  onImportFileClick = () => {
    const {tempOrganogram} = this.state

    if (tempOrganogram) {
      this.props.setOrganogram(tempOrganogram)
    }
  }

  // Renders
  renderInputFileSelector (hasOganogramFile: boolean) {
    return hasOganogramFile
      ? null
      : (
        <div className="row">
          <div className="col-lg-12 mt-1 mb-1 text-lg-center">
            <input
              type="file"
              id="inputFile"
              name="inputFile"
              accept=".json"
              onChange={this.onInputFileChange}
              />
          </div>
        </div>
      )
  }

  renderActionButtons (hasOganogramFile: boolean) {
    return hasOganogramFile
      ? [
        <Link
          key='check-organogram'
          className="btn btn-primary"
          to="organogram"
          >
          Go to my organogram
        </Link>,
        <button
          key='remove-button'
          type="button"
          className="btn btn-secondary"
          onClick={this.props.clearOrganogram}
          >
          Remove organogram file
        </button>
      ]
      : (
        <button
          type="button"
          className="btn btn-success"
          disabled={!this.state.tempOrganogram}
          onClick={this.onImportFileClick}
          >
          Save file
        </button>
      )
  }

  render () {
    const hasOganogramFile: boolean = !isEmptyObject(this.props.organogram)

    return (
      <div className="Upload">
        <div className="jumbotron mt-3 mb-3 text-lg-center">
          <h1>Manage organization file</h1>
          {hasOganogramFile
            ? (<p>Click on "Remove organogram file" to selected another file</p>)
            : (<p>Choose a JSON (.json) file to upload, then click "Save file"</p>)
          }

          {this.renderInputFileSelector(hasOganogramFile)}

          <div className="row">
            <div className="col-lg-12 mt-3 mb-3 text-lg-center action-buttons">
              {this.renderActionButtons(hasOganogramFile)}
            </div>
          </div>

          {this.state.userMessage &&
            (
              <div className="row">
                <div className="col-lg-12 text-lg-center">
                  <p className="alert-danger pt-3 pb-3">{this.state.userMessage}</p>
                </div>
              </div>
            )
          }
        </div>
      </div>
    )
  }
}
