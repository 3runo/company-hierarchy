/* @flow */
import React from 'react'
import { Link } from 'react-router'
import { connect } from 'react-redux'
import isEmptyObject from './../utility/isEmptyObject'

@connect(
  (state) => ({
    organogram: state.organogramReducer.organogram
  })
)
export default class Home extends React.Component {
  static propTypes = {
    organogram: React.PropTypes.object.isRequired
  }

  render () {
    const hasOganogramFile: boolean = !isEmptyObject(this.props.organogram)

    return (
      <div className="Home">
        <div className="jumbotron mt-3 mb-3 text-lg-center">
          <h1>Company Hierarchy</h1>
          <p className="lead">Welcome to our hierarchy manager. Through our tool, you can visualize the structure of your company in a more elegant way, as well as make decisions in a practical and fast way.</p>
          {hasOganogramFile
            ? (<Link className="btn btn-primary" to="organogram">Go to organogram</Link>)
            : (<Link className="btn btn-primary" to="upload">Select file</Link>)
          }
        </div>
      </div>
    )
  }
}
