/* @flow */
import React from 'react'
import Header from './Header'
import Footer from './Footer'

export default class App extends React.Component {
  static propTypes = {
    children: React.PropTypes.element
  }

  render () {
    return (
      <div className="App">
        <Header />

        <div className="container">
          {this.props.children}
        </div>

        <Footer />
      </div>
    )
  }
}
