/* @flow */
import React from 'react'
import { connect } from 'react-redux'
import { setOrganogram } from './../store/actions/'
import OrganogramAdjustable from './OrganogramAdjustable'
import isEmptyObject from './../utility/isEmptyObject'

import { getEmployeeListByName, removeEmployeeByName, pushEmployee } from './../utility/organogramHelpers'

@connect(
  (state) => ({
    organogram: state.organogramReducer.organogram
  }),
  { setOrganogram }
)
export default class Organogram extends React.Component {
  static propTypes = {
    organogram: React.PropTypes.object.isRequired,
    setOrganogram: React.PropTypes.func.isRequired
  }

  onOrganogramAdjustableUpdate = (draggedIndex: string, parentIndex: string) => {
    const { organogram } = this.props

    const swapEmployeeList = getEmployeeListByName(organogram, draggedIndex)
    if (!swapEmployeeList || !swapEmployeeList.length) {
      return
    }

    const organogramCleaned = removeEmployeeByName(organogram, draggedIndex)
    const newOrganogram = pushEmployee(organogramCleaned, swapEmployeeList[0], parentIndex)
    this.props.setOrganogram(newOrganogram)
  }

  render () {
    const hasOganogramFile: boolean = !isEmptyObject(this.props.organogram)

    return (
      <div className="Organogram">
        <div className="mt-3 mb-3 text-lg-center">
          <h1>Organogram</h1>
          <p>This is our hierarchy chart, the information presented here is according to the file that was previously imported</p>
        </div>
        {hasOganogramFile &&
          <OrganogramAdjustable
            organogram={this.props.organogram}
            onUpdate={this.onOrganogramAdjustableUpdate}
            />
        }
      </div>
    )
  }
}
