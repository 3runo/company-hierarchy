/* @flow */
import React from 'react'
import Employee from './Employee'
import { DragDropContext } from 'react-dnd'
import HTML5Backend from 'react-dnd-html5-backend'

class OrganogramAdjustable extends React.Component {
  static propTypes = {
    organogram: React.PropTypes.object.isRequired,
    onUpdate: React.PropTypes.func.isRequired
  }

  renderHierarchy (organogram) {
    return Object.keys(organogram)
      .map((name, i) =>
        <Employee
          key={i}
          index={name}
          name={name}
          position={organogram[name].position}
          employees={organogram[name].employees}
          onUpdate={this.props.onUpdate}
          />)
  }

  render () {
    return (
      <div className="OrganogramAdjustable">
        {this.renderHierarchy(this.props.organogram)}
      </div>
    )
  }
}

export default DragDropContext(HTML5Backend)(OrganogramAdjustable)
