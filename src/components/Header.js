/* @flow */
import React from 'react'
import { Link } from 'react-router'

export default class Header extends React.Component {
  render () {
    return (
      <div className="Header pos-f-t">
        <nav className="navbar navbar-light navbar-static-top bg-faded">
          <div className="container">
            <div className="collapse navbar-toggleable-xs" id="exCollapsingNavbar2">
              <Link className="navbar-brand" to="home">
                <img src="./img/personio-logo-web.png" alt=""/>
              </Link>
              <ul className="nav navbar-nav">
                <li className="nav-item">
                  <Link className="nav-link" activeClassName="active" to="home">Home</Link>
                </li>
                <li className="nav-item">
                  <Link className="nav-link" activeClassName="active" to="upload">Select file</Link>
                </li>
                <li className="nav-item">
                  <Link className="nav-link" activeClassName="active" to="organogram">Organogram</Link>
                </li>
              </ul>
            </div>
          </div>
        </nav>
      </div>
    )
  }
}
