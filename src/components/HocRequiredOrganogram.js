/* @flow */
import React from 'react'
import { connect } from 'react-redux'
import { browserHistory } from 'react-router'
import isEmptyObject from './../utility/isEmptyObject'

export default function HocRequiredOrganogram (ComponentClass: ReactClass<{}>) {
  class HocRequiredOrganogramClass extends React.Component {
    componentWillMount() {
      if (isEmptyObject(this.props.organogram)) {
        browserHistory.push('/upload')
      }
    }

    render () {
      return <ComponentClass {...this.props} />
    }
  }

  function mapStateToProps(state) {
    return { organogram: state.organogramReducer.organogram }
  }

  return connect(mapStateToProps)(HocRequiredOrganogramClass)
}