/* @flow */
import React from 'react'
import { Route, IndexRoute } from 'react-router'

import App from './components/App'
import Home from './components/Home'
import Organogram from './components/Organogram'
import Upload from './components/Upload'
import HocRequiredOrganogram from './components/HocRequiredOrganogram'

export default (
  <Route path="/" component={App}>
    <IndexRoute component={Home} />
    <Route path="home" component={Home} />
    <Route path="organogram" component={HocRequiredOrganogram(Organogram)} />
    <Route path="upload" component={Upload} />
  </Route>
)
